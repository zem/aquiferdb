import mysql.connector
import datetime
import os

package_directory = os.path.dirname(os.path.abspath(__file__))

cursor = []
connect = []
printqueries = False

def datetimestring2datetime(datetimestr):
	datestr, timestr = datetimestr.split()
	datesplit = datestr.split("-")
	year, month, day = map(int, datesplit)
	timesplit = timestr.split(":")
	hour, minute, second = map(int, timesplit)
	return datetime.datetime(year, month, day, hour, minute, second)

def datetime2datetimestring(dt):
	return datetime2datestring(dt) + " " + ":".join(map(lambda i: str(i).zfill(2), [dt.hour, dt.minute, dt.second]))

def datestring2datetime(datestr):
	splitstr = datestr.split("-")
	return datetime.datetime(*map(int, splitstr))

def datetime2datestring(dt):
	return "-".join(map(lambda i: str(i).zfill(2), [dt.year, dt.month, dt.day]))

def connecttodb():
	global cursor
	global connect
	creds = {"user": "perl", "password": "script", "host": "mads01.lanl.gov", "database": "LocalWork"}
	connect = mysql.connector.connect(**creds)
	cursor = connect.cursor()

def disconnectfromdb():
	global cursor
	global connect
	cursor.close()
	connect.close()

def setprintqueries(val):
	global printqueries
	printqueries = val

def getwaterlevels(observationwell, portdescr, observationbegintime, observationendtime):
	#get the water level information
	global cursor
	global printqueries
	query = "select time,ANY_VALUE(replace(format(piezometricWLft,9),',','')) from GroundWaterLevelData where wellname = '" + observationwell + "' and time <= '" + observationendtime + "' and time >= '" + observationbegintime + "' and (dataqualcode regexp '^$|V|VR|VQ|VRVQ' or dataqualcode is NULL)" + " and portdesc regexp '" + portdescr + "' and (probeno < 10 or probeno is NULL) and ANY_VALUE(replace(format(piezometricWLft,9),',','')) is not NULL group by time;"
	if printqueries:
		print query
	cursor.execute(query)
	waterleveltimes = []
	waterlevels = []
	for time, waterlevel in cursor:
		waterleveltimes.append(time)
		waterlevels.append(float(waterlevel) * 0.3048) #the 0.3048 is to convert from feet to meters
	return waterlevels, waterleveltimes

#get the average water level over a 1 month period
def getaveragewaterlevel(wellname, portdescr, year, month):
	global cursor
	global printqueries
	query = "select avg(piezometricWLft) FROM GroundWaterLevelData WHERE YEAR(time)=" + str(year) + " AND MONTH(time) = " + str(month) + " and 0 = strcmp(wellname, '" + wellname + "') and dataqualcode regexp '^$|V|VR|VQ|VRVQ' and portdesc regexp '" + portdescr + "' and probeno < 10;"
	if printqueries:
		print query
	cursor.execute(query)
	for wlavg in cursor:
		if wlavg[0] == None:
			wl = None
		else:
			wl = float(wlavg[0]) * 0.3048
	return wl

#get the production information
def getpumpingrates(productionwell, pumpingbegintime, pumpingendtime):
	global cursor
	global printqueries
	times = []
	rates = []
	"""
	if "PM" in productionwell and len(productionwell) == 4:
		productionwell = "PM-0" + productionwell[-1]
	if "O" in productionwell and len(productionwell) == 3:
		productionwell = "O-0" + productionwell[-1]
		"""
	query = "select time,productiongal from ProdDaily where strcmp(wellname, '" + productionwell + "') = 0 and time <= '" + pumpingendtime + "' and time >= '" + pumpingbegintime + "' order by time;"
	if printqueries:
		print query
	cursor.execute(query)
	i = 0
	for time, productiongal in cursor:
		productionmcubed = float(productiongal) / 264.172 #convert from gal to meters^3
		productionrate = productionmcubed #meters^3 / day
		times.append(time)
		rates.append(productionrate)
		i += 1
	if i > 0:#if the query succeeded, we need to add on a zero rate after on the day after the last time
		lasttime = times[-1]
		lasttime = lasttime + datetime.timedelta(days=1)
		times.append(lasttime)
		rates.append(0.)
	else:#if the query failed, let's look in the PumpTest table
		query = "select starttime,endtime,pumprategpm from PumpTest where strcmp(wellname, '" + productionwell + "') = 0 and starttime <= '" + pumpingendtime + "' and endtime >= '" + pumpingbegintime + "' order by starttime;"
		if printqueries:
			print query
		cursor.execute(query)
		lastendtime = []
		for starttime, endtime, pumprategpm in cursor:
			productionrate = pumprategpm * 60 * 24 / 264.172 #convert from gpm to meters^3 / day
			if lastendtime == starttime:
				#in this case, drop the zero we added as part of the end time on the last iteration
				del times[-1]
				del rates[-1]
			times.append(starttime)
			rates.append(productionrate)
			times.append(endtime)
			rates.append(0.)
			lastendtime = endtime
	return rates, times

#get the x, y, and radius of a produciton well
def getgeometry(wellname):
	"""
	if "PM" in wellname and len(wellname) == 5:
		wellname = "PM-" + wellname[-1]
	if "O" in wellname and len(wellname) == 4:
		wellname = "O-" + wellname[-1]
		"""
	query = "select inner_diam from ScreenData where strcmp(well_name, '" + wellname + "') = 0;"
	if printqueries:
		print query
	cursor.execute(query)
	for row in cursor:
		inner_diam = row[0]
                try:
                    radius = .5 * inner_diam * 0.0254 #convert from inches to meters
                except:
                    #if the diam isn't in the db, assume it is 9 inches
                    radius = .5 * 9 * 0.0254 #convert from inches to meters
	query = "select x_coord,y_coord from WQDBLocation where strcmp(location_name, '" + wellname + "') = 0;"
	if printqueries:
		print query
	cursor.execute(query)
	for x, y in cursor:
		x = x * 0.3048 #convert from feet to meters
		y = y * 0.3048
	return x, y, radius

def getscreenelevations(wellname):
	query = "select OPEN_TOP_DEPTH,OPEN_BOTTOM_DEPTH,surface_elevation from ScreenData inner join  WQDBLocation on well_name = location_name where 0 = strcmp(well_name, '" + wellname + "');"
	if printqueries:
		print query
	cursor.execute(query)
	topelevations = []
	bottomelevations = []
	for row in cursor:
		topelevations.append((row[2] - row[0]) * 0.3048)
		bottomelevations.append((row[2] - row[1]) * 0.3048)
	return topelevations, bottomelevations

#get the barometric information
def getbarometricpressure(barometricbegintime, barometricendtime):
	global cursor
	global printqueries
	query = "select time,pressmb from TA54BaroData where time >= '" + barometricbegintime + "' and time <= '" + barometricendtime + "' order by time;"
	if printqueries:
		print query
	cursor.execute(query)
	barotimes = []
	baropressmb = []
	for time,pressmb in cursor:
		if pressmb > 500:
			barotimes.append(time)
			if time < datetime.datetime(2014, 5, 21, 13, 45):
				baropressmb.append(pressmb - 1.5)
			else:
				baropressmb.append(pressmb + 1.5)
	return baropressmb, barotimes

#get the earth tide information -- for now, just read it from a file
def getearthtide(earthtidebegintime, earthtideendtime):
	#for now, it's not in the DB -- just read a file
	global cursor
	global printqueries
	with open(package_directory + "/earthtide.txt", "r") as f:
		lines = f.readlines()
		f.close()
	earthtidetimes = []
	earthtideforces = []
	begindt = datestring2datetime(earthtidebegintime)
	enddt = datestring2datetime(earthtideendtime)
	for line in lines:
		datetimestr, force = line.split(",")
		dt = datetimestring2datetime(datetimestr)
		if dt >= begindt and dt < enddt:
			earthtidetimes.append(dt)
			earthtideforces.append(float(force))
	return earthtideforces, earthtidetimes

def getannualchromiumconcentration(wellname, portdesc, year):
	global cursor
	global printqueries
	if portdesc == "SINGLE COMPLETION":
		locationname = wellname
	else:
		locationname = wellname + " S" + portdesc
	query = "select avg(result) from WellsAnalyses where (0 = strcmp(analyte, 'Chromium') or 0 = strcmp(analyte, 'Chromium hexavalent ion')) and 0 = strcmp(location, '" + locationname + "') and collection_date_time >= '" + str(year) + "-01-01 00:00:00' and collection_date_time < '" + str(year + 1) + "-01-01 00:00:00';"
	if printqueries:
		print query
	cursor.execute(query)
	for resultavg in cursor:
		if resultavg[0] == None:
			result = None
		else:
			result = float(resultavg[0])
	return result

def getchromiumconcentrations(wellname, portdesc, fullchemdb=False, filterresults=True):
	global cursor
	global printqueries
	if portdesc == "SINGLE COMPLETION":
		locationname = wellname
	else:
		locationname = wellname + " S" + portdesc

	if fullchemdb:
		mytable = "AnalyticalData"
		parameter = "Chromium"
		fields = ["LOCATION_ID",
				"SAMPLE_DATE",
				"SAMPLE_TIME",
				"PARAMETER_NAME",
				"ANALYSIS_TYPE_CODE",
				"SAMPLE_PURPOSE",
				"FIELD_PREPARATION_CODE",
				"DETECT_FLAG",
				"REPORT_RESULT",
				"REPORT_UNITS",
				"FIELD_SAMPLE_ID",
				"DILUTION_FACTOR",
				"BEST_VALUE_FLAG",
				"LAB_ID",
				"SAMPLE_USAGE_CODE"]

		query = "select " + ', '.join(fields) + " from " + mytable + " where LOCATION_ID=\'" + locationname + "\' and " + "PARAMETER_NAME=\'" + parameter + "\';"
                print query

		# get the data
		cursor.execute(query)

		# arrange the data
		temp = []
		rawresults = []
		for resultanddt in cursor:
			if resultanddt == None:
				result = None
			else:
				for i in range(0,len(fields)):
					# print(resultanddt[i])
					temp.append(resultanddt[i])
				rawresults.append(temp)
				temp = []

                if filterresults:
                    # filter the data
                    idetect = fields.index("DETECT_FLAG")
                    ibestvalue = fields.index("BEST_VALUE_FLAG")
                    ipurpose = fields.index("SAMPLE_PURPOSE")
                    iusagecode = fields.index("SAMPLE_USAGE_CODE")
                    ilabid = fields.index("LAB_ID")
                    itypecode = fields.index("ANALYSIS_TYPE_CODE")
                    ifieldprepcode = fields.index("FIELD_PREPARATION_CODE")
                    results_filtered = []
                    for i in range(0,len(rawresults)):
                            if  rawresults[i][idetect]=="Y" and rawresults[i][ibestvalue]=="Y" and rawresults[i][ipurpose]=="REG" and rawresults[i][iusagecode]=="INV" and rawresults[i][ilabid]=="GELC" and rawresults[i][itypecode]=="INIT" and rawresults[i][ifieldprepcode]=="F":
                            #if  rawresults[i][idetect]=="Y" and rawresults[i][ibestvalue]=="Y" and rawresults[i][ipurpose]=="REG" and rawresults[i][iusagecode]=="INV" and rawresults[i][itypecode]=="INIT" and rawresults[i][ifieldprepcode]=="F":
                                    results_filtered.append(rawresults[i])
                else:
                    results_filtered = rawresults

		# grab the filtered results
		iresult = fields.index("REPORT_RESULT")
		idate = fields.index("SAMPLE_DATE")
		itime = fields.index("SAMPLE_TIME")
		results = []
		thedates = []
		thetimes = []
		for result_filtered in results_filtered:
			results.append(float(result_filtered[iresult]))
			thedates.append(result_filtered[idate])
			thetimes.append(result_filtered[itime])

		# combine time and datetime for dts
		dts = []
		for i in range(0,len(thedates)):
			dts.append(datetime.datetime.combine(thedates[i],datetime.datetime.strptime(thetimes[i],"%H:%M").time()))

	else:
		query = "select result,collection_date_time from WellsAnalyses where (0 = strcmp(analyte, 'Chromium') or 0 = strcmp(analyte, 'Chromium hexavalent ion')) and 0 = strcmp(location, '" + locationname + "') order by collection_date_time;"
		if printqueries:
			print query
		cursor.execute(query)
		results = []
		dts = []
		for resultanddt in cursor:
			if resultanddt == None:
				result = None
			else:
				results.append(float(resultanddt[0]))
				dts.append(resultanddt[1])

	return results, dts
